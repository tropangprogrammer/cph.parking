﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CPh.Parking.Web.Models
{
    public class Vehicle
    {
        public int Id { get; set; }

        public string PlateNumber { get; set; }

        [Required]
        [Display (Name = "Class")]
        public string CarC { get; set; }
        
       // public virtual User User { get; set; }


    }
}

