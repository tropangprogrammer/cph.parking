﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CPh.Parking.Web.Models
{
    public class Viewing
    {
        
        public int Id { get; set; }

       // public virtual User User { get; set; }

        [Required]
        [Display(Name = "Global ID")]
        public string GlobalID { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Floor")]
        public int Floor { get; set; }

        [Display(Name = "IP Phone")]
        public string IP { get; set; }




    }
}
