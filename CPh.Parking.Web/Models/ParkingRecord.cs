﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CPh.Parking.Web.Models
{
    public class ParkingRecord
    {
        public int Id { get; set; }

        [Required]
        [Display(Name ="Slot Number")]
        public int SlotNo { get; set; }

        [Display(Name = "Full Name")]
        public string FN { get; set; }

        [Display(Name = "Date")]
        [DataType (DataType.Date)]
        public string Date { get; set; }

        [Display (Name ="Time In")]
        [DataType(DataType.Time)]
        public DateTime? TimeIn { get; set; } 
        
        [Display(Name = "Time Out")]
        [DataType(DataType.Time)]
        public DateTime? TimeOut { get; set; }

        public virtual Vehicle Vehicle { get; set; }
        public virtual Slot Slot { get; set; }
      
    }
}
