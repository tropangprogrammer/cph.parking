﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CPh.Parking.Web.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.IISIntegration;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CPh.Parking.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddDbContext<ParkingDbContext>(options =>
                options.UseSqlite(Configuration.GetConnectionString("SqliteConnection")));

           // services.AddAuthentication(IISDefaults.AuthenticationScheme);

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = IISDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = IISDefaults.Ntlm;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, 
                               IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });   

            //No migrations
            using (var scope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>()
                .CreateScope())
            {
                scope.ServiceProvider.GetService<ParkingDbContext>()
                    .Database.EnsureCreated();
            }
        }
    }
}
