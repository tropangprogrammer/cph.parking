﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CPh.Parking.Web.Models;
using Microsoft.AspNetCore.Authorization;

namespace CPh.Parking.Web.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return RedirectToAction("ParkingSlots");
        }

        //public IActionResult About()
        //{
        //    ViewData["Message"] = "Your application description page.";

        //    return View();
        //}

        //[Authorize(Roles = "CPh-F-ORG_HMR_Dispatch_Gcp-M,CPh-F-ORG_Itm_Pit_EngIt_Im-R")]
        //public IActionResult Contact()
        //{
        //    ViewData["Message"] = "Your contact page.";

        //    return View();
        //}

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult ParkingSlots()
        {
            return View();
        }
    }
}
