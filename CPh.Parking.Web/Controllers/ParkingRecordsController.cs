﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CPh.Parking.Web.Data;
using CPh.Parking.Web.Models;

namespace CPh.Parking.Web.Controllers
{
    public class ParkingRecordsController : Controller
    {
        private readonly ParkingDbContext _context;

        public ParkingRecordsController(ParkingDbContext context)
        {
            _context = context;
        }

        // GET: ParkingRecords
        public async Task<IActionResult> Index()
        {
            return View(await _context.ParkingRecords.ToListAsync());
        }

        // GET: ParkingRecords/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var parkingRecord = await _context.ParkingRecords
                .SingleOrDefaultAsync(m => m.Id == id);
            if (parkingRecord == null)
            {
                return NotFound();
            }

            return View(parkingRecord);
        }

        // GET: ParkingRecords/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ParkingRecords/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,TimeIn,TimeOut")] ParkingRecord parkingRecord)
        {
            if (ModelState.IsValid)
            {
                _context.Add(parkingRecord);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(parkingRecord);
        }

        // GET: ParkingRecords/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var parkingRecord = await _context.ParkingRecords.SingleOrDefaultAsync(m => m.Id == id);
            if (parkingRecord == null)
            {
                return NotFound();
            }
            return View(parkingRecord);
        }

        // POST: ParkingRecords/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,TimeIn,TimeOut")] ParkingRecord parkingRecord)
        {
            if (id != parkingRecord.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(parkingRecord);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ParkingRecordExists(parkingRecord.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(parkingRecord);
        }

        // GET: ParkingRecords/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var parkingRecord = await _context.ParkingRecords
                .SingleOrDefaultAsync(m => m.Id == id);
            if (parkingRecord == null)
            {
                return NotFound();
            }

            return View(parkingRecord);
        }

        // POST: ParkingRecords/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var parkingRecord = await _context.ParkingRecords.SingleOrDefaultAsync(m => m.Id == id);
            _context.ParkingRecords.Remove(parkingRecord);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ParkingRecordExists(int id)
        {
            return _context.ParkingRecords.Any(e => e.Id == id);
        }
    }
}
