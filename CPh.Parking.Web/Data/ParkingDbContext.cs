﻿using CPh.Parking.Web.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CPh.Parking.Web.Data
{
    public class ParkingDbContext : DbContext
    {

        public DbSet<User> Users { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }

        [Display(Name="Parking Record")]
        public DbSet<ParkingRecord> ParkingRecords { get; set; }
        public DbSet<Slot> Slots { get; set; }
        
        public ParkingDbContext(DbContextOptions<ParkingDbContext> options) : base(options)
        {

        }
    }
}
